function addFormItem(id, data) {
	if (typeof data == "undefined") {
		data = {};
	};
	data.id = id;
	var source = $("#item-template").html();
	var template = Handlebars.compile(source);
	var html = template(data);
	$('#items-container').append(html);
}

function removeFormItem(id) {
	$("#item-"+id).remove();
}

$(document).on('click', '#add-item', function() {
	var itemId = parseInt($('#items-container').attr('data-items'))+1;
	addFormItem(itemId);
	$('#items-container').attr('data-items', itemId);
});

$(document).on('click', '.remove-item', function(){
	removeFormItem($(this).attr('data-item-id'));
});

$(document).on('change', '#from-hotel', function(){
	var company_id = $('option:selected', this).attr('data-company');
	$('#from-company').val(company_id);
});

$(document).on('change', '#to-hotel', function(){
	var company_id = $('option:selected', this).attr('data-company');
	$('#to-company').val(company_id);
});

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
	if (!$.isEmptyObject(document.items)) {
		removeFormItem(1);
		for (i in document.items) {
			addFormItem(i, document.items[i]);
		}
	};

	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        $("#filename").html(label);
    });
});