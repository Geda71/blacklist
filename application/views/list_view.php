 <!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('header'); ?>
</head>
<body>
<div id="wrapper">
	<?php $this->load->view('menu') ?>
	<div id="page-wrapper">
		<div class="page-header">
	        <h1 class="centered">Not to be hired employees / shops&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/lists/add" class="btn btn-info">+ &nbsp;add</a></h1>

	    </div>
	    <?php if (isset($message)): ?>
	    	<div class="alert alert-<?php echo $message['type']; ?>">
	        	<strong><?php echo $message['head']; ?></strong>
	        	<p><?php echo (isset($message['body'])) ? $message['body'] : ''; ?></p>
	    	</div>
	    <?php endif ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 data-container">

			<div class="pager tablesorter-pager" style="display: block;">
			    Page: <select class="form-control gotoPage pager-filter" aria-disabled="false"></select>
			    <i class="fa fa-fast-backward pager-nav first disabled" alt="First" title="First page" tabindex="0" aria-disabled="true"></i>
			    <i class="fa fa-backward pager-nav prev disabled" alt="Prev" title="Previous page" tabindex="0" aria-disabled="true"></i>
			    <span class="pagedisplay"></span>
			    <i class="fa fa-forward pager-nav next" alt="Next" title="Next page" tabindex="0" aria-disabled="false"></i>
			    <i class="fa fa-fast-forward pager-nav last" alt="Last" title="Last page" tabindex="0" aria-disabled="false"></i>
			    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Page size:</span>
			    <select class="form-control pagesize pager-filter" aria-disabled="false">
			      <option value="10">10</option>
			      <option selected="selected" value="30">30</option>
			      <option value="50">50</option>
			    </select>
			  </div>

				<div class="table-responsive">
			  <a href="#" class="hidden reset-filters btn btn-warning">Reset</a>
			    <table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th>Hotel Name<i class="fa fa-sort"></i></th>
						<th>Position / Shop<i class="fa fa-sort"></th>
						<th>Employee name<i class="fa fa-sort"></th>
						<th>photo<i class="fa fa-sort"></th>
						<th>National ID<i class="fa fa-sort"></th>
						<th>Incident date<i class="fa fa-sort"></th>
						<th>Leaving reason<i class="fa fa-sort"></th>
						<th>Termination date<i class="fa fa-sort"></th>
						<th>Created by<i class="fa fa-sort"></th>
						<th>Department<i class="fa fa-sort"></th>
						<th>Comments<i class="fa fa-sort"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($lists as $list): ?>
						<tr data-id="<?php echo $list['id']; ?>">
							<td><?php echo $list['hotel_name']; ?></td>
							<td><?php echo $list['position']; ?></td>
							<td><?php echo $list['name']; ?></td>
							<td>
							<?php if (!is_null($list['photo'])): ?>
								<img class="personal-photo" src="<?php echo $photo_path.$list['photo']; ?>" />
							<?php else: ?>
								No Photo
							<?php endif ?>
							</td>
							<td><?php echo $list['national_id']; ?></td>
							<td><?php echo $list['incident_date']; ?></td>
							<td><?php echo $list['reason']; ?></td>
							<td><?php echo $list['termination_date']; ?></td>
							<td><?php echo $list['fullname']; ?></td>
							<td><?php echo $list['department_name']; ?></td>
							<td><?php echo $list['comments']; ?></td>
						</tr>
					<?php endforeach ?>
					
				</tbody>
			</table>
		</div>
		<script type="text/javascript">
		    $(function(){

		        // define pager options
		        var pagerOptions = {
		          // target the pager markup - see the HTML block below
		          container: $(".pager"),
		          // output string - default is '{page}/{totalPages}'; possible variables: {page}, {totalPages}, {startRow}, {endRow} and {totalRows}
		          output: '{startRow} - {endRow} / {filteredRows} ({totalRows})',
		          // if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
		          // table row set to a height to compensate; default is false
		          fixedHeight: true,
		          // remove rows from the table to speed up the sort of large tables.
		          // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
		          removeRows: false,
		          // go to page selector - select dropdown that sets the current page
		          cssGoto: '.gotoPage'
		        };

		        // Initialize tablesorter
		        // ***********************
		        $("table")
		          .tablesorter({
		            theme: 'bootstrap',
		            headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
		            widthFixed: true,
		            widgets: ['filter'],
		            widgetOptions: {
		              filter_reset : '.reset-filters',
		              filter_functions: {
		                0: {
		                  <?php foreach ($hotels as $hotel) :?>
		                  "<?php echo $hotel['name']; ?>":function(e, n, f, i, $r) { return f == e; },
		                  <?php endforeach; ?>
		                },
		                3: "disabled",
		                6: {
		                  <?php foreach ($reasons as $reason) :?>
		                  "<?php echo $reason['name']; ?>":function(e, n, f, i, $r) { return f == e; },
		                  <?php endforeach; ?>
		                },
		                9: {
		                  <?php foreach ($departments as $department) :?>
		                  "<?php echo $department['name']; ?>":function(e, n, f, i, $r) { return f == e; },
		                  <?php endforeach; ?>
		                }
		              }
		            }
		          })

		          // initialize the pager plugin
		          // ****************************
		          .tablesorterPager(pagerOptions)
		          .find(".tablesorter-filter-row td:last input").replaceWith('<a href="#" class="reset-filters btn btn-warning">Reset</a>');
		      });
		  </script>
		</div>
	</div>
</div>
</div>
</body>
</html>