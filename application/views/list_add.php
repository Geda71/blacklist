<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('header'); ?>
</head>
<body>
<div id="wrapper">
	<?php $this->load->view('menu') ?>
	<div id="page-wrapper">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<fieldset>
			<legend>Add employee to blacklist</legend>

			<?php if(validation_errors() != false): ?>
				<div class="alert alert-danger">
					<?php echo validation_errors(); ?>
				</div>
			<?php endif ?>

			<form action="" method="POST" id="form-submit" class="form-div span12" accept-charset="utf-8" enctype="multipart/form-data">


				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="hotel" class="col-lg-2 col-md-2 col-sm-2 col-xs-2  control-label">Hotel</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<select class="form-control" name="hotel" id="hotel">
							<option data-company="0" value="">Select Hotel..</option>
							<?php foreach ($hotels as $hotel): ?>
								<option data-company="<?php echo $hotel['company_id']; ?>" value="<?php echo $hotel['id'] ?>" <?php echo set_select('hotel',$hotel['id'] ); ?>><?php echo $hotel['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<label for="department" class="col-lg-offset-2 col-md-offset-0 col-sm-offset col-xs-offset-0 position-lg-2 col-md-2 col-sm-2 col-xs-2  control-label">Department</label>
						<div class="col-lg-3 col-md-4 col-sm-5 col-xs-5">
							<select class="form-control" name="department" id="department">
								<option value="">Select Department..</option>
								<?php foreach ($departments as $department): ?>
									<option value="<?php echo $department['id'] ?>"<?php echo set_select('department', $department['id']); ?>><?php echo $department['name']; ?></option>
								<?php endforeach ?>
							</select>
						</div>
				</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="incident-date" class="col-lg-2 col-md-2 col-sm-2 col-xs-2  control-label">Incident Date</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<input type="text" class="form-control datetime" name="incident-date" id="incident-date" data-date-format="YYYY-MM-DD" value="<?php echo set_value('incident-date'); ?>"></input>
					</div>

					<label for="termination-date" class="col-lg-offset-2 col-md-offset-0 col-sm-offset col-xs-offset-0 col-lg-2 col-md-2 col-sm-2 col-xs-2  control-label">Termination Date</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<input type="text" class="form-control datetime" name="termination-date" id="termination-date" data-date-format="YYYY-MM-DD" value="<?php echo set_value('termination-date') ?>"></input>
					</div>
					<script type="text/javascript">
					$(function () {
							$('.datetime').datetimepicker({
								autoclose: true,
								pickTime: false,

							});
							
					});
					</script>
				</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="name" class="col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label">Name</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" ></input>
					</div>


					<label for="national-id" class="col-lg-offset-2 col-md-offset-0 col-sm-offset col-xs-offset-0 col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label">National ID</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<input type="text" class="form-control" name="national-id" id="national-id" value="<?php echo set_value('national-id'); ?>" ></input>
					</div>

				</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="incident-date" class="col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label">Photo</label>
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<span class="btn btn-default btn-file">
							Browse... <input type="file" class="form-control" name="photo" id="photo" value="<?php echo set_value('photo'); ?>" />
							<span id="filename"></span>
						</span>
					</div>
					<label for="position" class="col-lg-offset-2 col-md-offset-0 col-sm-offset col-xs-offset-0 col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label">Position / Shop</label>
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
						<input type="text" class="form-control" name="position" id="position" value="<?php echo set_value('position'); ?>" ></input>
					</div>
				</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="reason_id" class="col-lg-3 col-md-3 col-sm-3 col-xs-3  control-label">Termination Reason</label>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<select class="form-control" name="reason_id" id="reason_id">
							<option value="">Select Reason..</option>
							<?php foreach ($reasons as $reason): ?>
								<option value="<?php echo $reason['id'] ?>" <?php echo set_select('reason_id',$reason['id'] ); ?>><?php echo $reason['name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label for="comments" class="col-lg-4 col-md-4 col-sm-4 col-xs-4  control-label">Comments</label>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<textarea class="form-control" name="comments" id="comments"><?php echo set_value('comments'); ?></textarea>
					</div>
				</div>
				
				<div class="form-group">
				    <div class="col-lg-offset-3 col-lg-10 col-md-10 col-md-offset-3">
				    	<input name="submit" value="Submit" type="submit" class="btn btn-success"></input>
				    	<a href="<?= base_url(); ?>lists" class="btn btn-warning">Cancel</a>
				    </div>
				</div>

			</form>
			</fieldset>
		</div>
	</div>
	</div>
</div>
</body>
</html>
