<?php /*<div class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">e-Signature</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <?php if (isset($username)): ?>
            <li><a href="/forms/submit">New Form</a></li>
            <li><a href="/forms">View Forms</a></li>
        <?php endif; ?>
        <?php if (isset($is_admin) && $is_admin): ?>
          <li><a href="/backend">Settings</a></li>
        <?php endif ?>
        <?php if (isset($username)): ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><span class="glyphicon-class"><?php echo $username; ?><b class="caret"></b></span></a>
            <ul class="dropdown-menu">
              <li><a href="/auth/change_password">Change Password</a></li>
              <li class="divider"></li>
              <li class="dropdown-header"></li>
              <li><a href="/auth/logout">Logout</a></li>
            </ul>
          </li>
        <?php else: ?>
          <li><a href="/auth/login">Login</a></li>
        <?php endif; ?>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
*/?>

<!-- Sidebar -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">Blacklist</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">

    <ul class="nav navbar-nav navbar-right navbar-user">
    <?php if (isset($is_admin) && $is_admin): ?>
      <li><a href="/backend" class="top-sm-btn"><i class="fa fa-cogs"></i> Settings</a></li>
    <?php endif ?>
    <?php if (isset($username)): ?>
      <li class="dropdown user-dropdown">
        <a href="#" class="dropdown-toggle top-sm-btn" data-toggle="dropdown"><i class="fa fa-user glyphicon glyphicon-user"></i> <?php echo $username; ?><b class="caret"></b></span></a>
        <ul class="dropdown-menu">
          <li><a href="/auth/change_password"><i class="fa fa-lock"></i> Change Password</a></li>
          <li class="divider"></li>
          <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
        </ul>
      </li>
    <?php else: ?>
      <li><a href="/auth/login"><i class="fa fa-users"></i> Login</a></li>
    <?php endif; ?>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>