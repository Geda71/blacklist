<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends CI_Controller {
	private $data;
	public function __construct() {
		parent::__construct();
		$this->load->library('Tank_auth');
		if (!$this->tank_auth->is_logged_in()) {
			$redirect_path = '/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);
			$this->session->set_flashdata('redirect', $redirect_path);
			redirect('/auth/login');
		} else {
			$this->data['user_id'] = $this->tank_auth->get_user_id();
			$this->data['username'] = $this->tank_auth->get_username();
			$this->data['is_admin'] = $this->tank_auth->is_admin();
		}
		$this->data['menu']['active'] = "lists";
		$this->data['photo_path'] = 'assets/uploads/files/';
	}
	
	public function index() {
		$this->load->model('lists_model');
		$this->load->model('hotels_model');
		$this->load->model('departments_model');
		$this->load->model('reasons_model');
		$this->data['lists'] = $this->lists_model->getall();
		$this->data['hotels'] = $this->hotels_model->getall();
		$this->data['departments'] = $this->departments_model->getall();
		$this->data['reasons'] = $this->reasons_model->getall();
		$this->load->view('list_view', $this->data);
	}

	function do_upload($field)
	{
		$config['upload_path'] = 'assets/uploads/files/';
		$config['allowed_types'] = 'gif|jpg|png';
		
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($field))
		{
			$this->data['error'] = array('error' => $this->upload->display_errors());
		}
		else
		{
			$file = $this->upload->data();
			return $file['file_name'];

		}
	}
		
	public function add() {

 
		if ($this->input->post('submit')) {

			$photo_name = $this->do_upload('photo');

			$this->load->library('form_validation');

			$this->form_validation->set_rules('hotel','Hotel','trim|required');
			$this->form_validation->set_rules('position','Position','trim');
	    	$this->form_validation->set_rules('incident-date','Incident date','trim|required');
	    	$this->form_validation->set_rules('termination-date','Termination date','trim|required');
	    	$this->form_validation->set_rules('name','Name','trim|required');
	    	// if (empty($_FILES['photo']['name'])) {
		    // 	$this->form_validation->set_rules('photo','Photo','required');
		    // }
	    	$this->form_validation->set_rules('department','Department','trim|required');
	    	$this->form_validation->set_rules('reason_id','Reason','trim|required');
	    	$this->form_validation->set_rules('comments','Comments','trim');

	    	if ($this->form_validation->run() == TRUE) {
	    		$this->load->model('lists_model');

	    		$list_data = array(
	    							'user_id' => $this->data['user_id'],
	    							'hotel_id' => $this->input->post('hotel'),
	    							'position' => $this->input->post('position'),
	    							'photo' => $photo_name,
	    							'name' => $this->input->post('name'),
	    							'national_id' => $this->input->post('national-id'),
	    							'incident_date' => $this->input->post('incident-date'),
	    							'termination_date' => $this->input->post('termination-date'),
	    							'department_id' => $this->input->post('department'),
	    							'reason_id' => $this->input->post('reason_id'),
	    							'comments' => $this->input->post('comments')
	    							);
	    		$list_id = $this->lists_model->create($list_data);
	    		if (!$list_id) {
	    			die("ERROR");//@TODO failure view
	    		}
	    		redirect('/lists/');
	    	}

			

		}

		try {

			$this->load->helper('form');
			$this->load->model('hotels_model');
			// $this->load->model('companies_model');
			$this->load->model('departments_model');
			// $this->load->model('roles_model');
			$this->load->model('reasons_model');
			$this->data['hotels'] = $this->hotels_model->get_user_hotels($this->data['user_id']);
			// $this->data['companies'] = $this->companies_model->getall();
			$this->data['departments'] = $this->departments_model->getall();
			// $this->data['roles'] = $this->hotels_model->getall();
			$this->data['reasons'] = $this->reasons_model->getall();

			$this->load->view('list_add',$this->data);
		}
		catch( Exception $e) {
			show_error($e->getMessage()." _ ". $e->getTraceAsString());
		}
	}

}
