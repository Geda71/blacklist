<?php
class Lists_model extends CI_Model{
	
  	function __contruct(){
		parent::__construct;
	}

	function getall() {
  	  	$this->load->database();

  	  	$this->db->select('lists.id,
  	  		hotels.name AS hotel_name,
  	  		lists.position,
  	  		lists.name,
  	  		lists.national_id,
  	  		lists.photo,
  	  		lists.incident_date,
  	  		lists.termination_date,
  	  		reasons.name AS reason,
  	  		departments.name AS department_name,
  	  		users.fullname,
  	  		lists.comments
  	  		');
  	  	$this->db->join('hotels','hotels.id = lists.hotel_id', 'left');
  	  	$this->db->join('reasons','reasons.id = lists.reason_id', 'left');
  	  	$this->db->join('departments','departments.id = lists.department_id', 'left');
  	  	$this->db->join('users','users.id = lists.user_id', 'left');


		$query = $this->db->get('lists');
		return $query->result_array();
	}

	function create($data) {
		$this->db->insert('lists', $data);

		return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;

	}
}
?>
