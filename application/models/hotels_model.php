<?php

class Hotels_model extends MY_Model{
	
  	function __contruct(){
		parent::__construct;
		// $this->load->helper('url');
  	}
	
  	function getall(){
	    $this->load->database();
		
		$query = $this->db->get('hotels');
		return $query->result_array();
  	}

  	function get_user_hotels($uid){
	    $this->load->database();

		$query = $this->db->query("SELECT hotels.id, hotels.name, hotels.company_id FROM hotels
									JOIN employees_hotels ON hotels.id = employees_hotels.hotel_id
									JOIN users ON employees_hotels.employee_id = users.id
									WHERE users.id = {$uid}");

		return $query->result_array();
  	}
}
?>
